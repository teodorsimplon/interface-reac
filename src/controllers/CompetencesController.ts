import { Request, Response } from "express-serve-static-core";


export default class CompetencesController {
    static competences(req: Request, res: Response): void {
        //On appelle la DB dans la fonction 
        const db = req.app.locals.db;
        //On y récupère tous les elements de la table compétence 
        //en l'affectant à la variable comp 
        const comp = db.prepare("SELECT * FROM competences").all();

        //On demande un rendu de cette logique sur le chemin 'pages/competences'
        res.render('pages/competences', {
            title: 'Competences',
            arrayComp: comp
        });
    }
}
