// import express
import { Request, Response } from "express-serve-static-core";

// import CompetenceController
import CompetencesController from "./CompetencesController";

//export de CriteresController
export default class CriteresController {

    /**
     * affiche les critères
     * @param req 
     * @param res 
     */
    static criteres(req: Request, res: Response): void {
        // Récupère la DB 
        const db = req.app.locals.db;
        //Récupère le params.id
        const id = req.params.id;
        // On défini une constante "criteres" qui récupère depuis la BDD 
        // les ID des Compétences depuis la table criteres
        const criteres = db.prepare("SELECT * FROM criteres WHERE Competences_ID=?").all(id);
        // On défini une constante test qui récupère la table Criteres_ID  
        // depuis la table pivot user_has_Criteres mais seulement les User_ID
        let test = db.prepare("SELECT Criteres_ID FROM user_has_Criteres WHERE User_ID=?").all(1);

        //On transforme l'objet en tableau
        const criteresArray = test.map((e: any) => e.Criteres_ID)

        //LOGIQUE DU CHECKED
        criteres.forEach((element: any) => {
            if (criteresArray.includes(element.ID)) {
                element.checked = "checked"
            }
            else { element.checked = "" }
        });

        //RENDU DE LA PAGE SUR CHEMIN: "PAGES/CRITERES"
        res.render('pages/criteres', {
            title: 'Criteres',
            arrayCri: criteres
        });
    };

    /**
     * Recupère et traite les données du formulaire
     * @param req 
     * @param res 
     */
    static formulaire(req: Request, res: Response): void {
        // On appelle la DB dans la fonction en cours
        const db = req.app.locals.db;
        // On crée une variable criteres qui va chercher dans la DB 
        // tout le contenu de Competence_ID se trouvant dans la table criteres 
        let criteres = db.prepare("SELECT * FROM criteres WHERE Competences_ID=?").all(req.body.compId);

        // START DE LA LOGIQUE DE DECOCHAGE 
        //1. on doit transomer le contenu recuperé dans la variable critere en tableau 
        criteres = criteres.map((e: any) => e.ID)

        criteres.forEach((id: any) => {
            if (req.body[id]) {
                console.log(req.session);
                // Verifier si present en DB
                //@ts-ignore
                var idCri = db.prepare("SELECT * FROM  user_has_Criteres WHERE Criteres_ID = ? AND user_ID=?").get(id, req.session.userID);
                // Verifier si doublon en DB 
                if (idCri === undefined) {
                    db.prepare("INSERT OR REPLACE INTO user_has_Criteres (user_ID, Criteres_ID) VALUES (?, ?)").run(req.session.userID, id);
                }
            }
            else {
                //=>delete les elements en bdd  
                db.prepare("DELETE FROM user_has_Criteres WHERE Criteres_ID = ? AND user_ID = ?").run(id, req.session.userID)
            }
        });
        CompetencesController.competences(req, res)
    }
}
