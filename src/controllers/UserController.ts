import { Request, Response } from "express-serve-static-core";
import { nextTick } from "process";
import CompetencesController from "./CompetencesController";
import HomeController from "./HomeController";
import bcrypt, { hash } from 'bcrypt';
export default class UserController {
//affiche la page register
    static RegForm(req: Request, res: Response): void {
        res.render('pages/register');
    }

//stock les donnees d'utilisateur dans la base de donnee    
    static register(req: Request, res: Response): void {
        

        const db = req.app.locals.db;

        // console.log(req.body);
        const hash = bcrypt.hashSync(req.body.password, 10);
        // console.log(hash);
        db.prepare('INSERT INTO user ( "username", "email" , "password") VALUES ( ?, ?, ?)').run(req.body.username, req.body.email, hash);

        UserController.LogForm(req, res);
    }
// quand le process d'eregistrement est fini il est evoie vers la page de login    

    static LogForm(req: Request, res: Response): void {
        // console.log(req.session)
        res.render('pages/login');
    }
// affiche la page login et demands utilisateur pour un nom d'utilisateur et un mots de passe    

    static login(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const username = req.body.username;
        const password = req.body.password;

// le nom d'utilisateur et mot de passe est comparee avec les donnees du database dans le tableau user et s'il existe est il correspond il est dirigee vers le index si no vers le login
        const user = db.prepare('SELECT * FROM user WHERE username = ? ').get( username);
        // console.log('user exists ?: ', user)
        // console.log(req.session);

        if (user) { 
            if (bcrypt.compareSync(req.body.password, user.password)){
                
                req.session.userID=user.ID
                console.log(req.session);
                CompetencesController.competences(req, res);
                
                
            }
            else {
                UserController.LogForm(req, res);
            }
        }
        else {
            UserController.LogForm(req, res);
            
            
        }
    }
    static logout(req: Request, res: Response): void {

    }

    // static redirectLogin(req: Request, res: Response): void {
    //     if (!req.session.user.ID) {
    //         res.redirect('/login')
    //     } else {
    //         next()
    //     }


    // } 


}











function ID(ID: any, username: any, password: any) {
    throw new Error("Function not implemented.");
}

function password(password: any, arg1: number) {
    throw new Error("Function not implemented.");
}

