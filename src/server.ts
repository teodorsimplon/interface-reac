// Import dependencies
import path from "path";
import fs from 'fs';
import express from 'express';
import bodyParser from 'body-parser';
import Database from 'better-sqlite3';
const { config, engine } = require('express-edge');
import session from 'express-session';
import cookieParser from "cookie-parser";
import bcrypt from 'bcrypt';




// Import router
import router from './router';

declare module 'express-session' {
  interface Session
  {
      userID: number; // <-- ici mettez ce que vous voulez rajouter a votre session
  }
}

// Initialize the express engine
const app: express.Application = express();

// Take a port 3000 for running server.
const port: number = 3000;

const db = new Database(path.join(__dirname, './database/data.db'), { verbose: () => { console.log } });
app.locals.db = db;
/**
 * creation de la base de données, et insertion des données
 * A ne lancer qu'une seule fois
 */
//const structure = fs.readFileSync(path.join(__dirname, '/database/interface_reac_database_Structure.sql'), 'utf8');
//db.exec(structure);
//const data = fs.readFileSync(path.join(__dirname, '/database/database_interface_reac.sql'), 'utf8');
//db.exec(data);


app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    //cookie validity set for 2hrs
    cookie: { maxAge: 1000 * 60 * 60 * 2 }
    
  }))


// define the templating engine
app.use(engine);

// define the Views folder
app.set("views", path.join(__dirname, "./views"));

app.use(
    express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);

// to read request form body
app.use(bodyParser.urlencoded({ extended: false }));

  //parsing incoming user data 

  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  
  // cookie parser middleware
  app.use(cookieParser());

  // app.use(function (req, res, next) {
  //   if (!req.session.user.ID) {
  //     req.session.user.ID = {}
  //   }


  // function that used to restrict access to certain routes links
export const redirectLogin = (req: Request, res: any , next: any ) => {
   //@ts-ignore
  if (!req.session.userID) {
    res.redirect('/login')
  } else {
    next()
  }
}
// Routes
router(app);



// Server setup
app.listen(port, () => {
    console.log(`TypeScript with Express http://localhost:${port}/`);
});

//Form

//User management



