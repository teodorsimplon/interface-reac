import { Application } from "express";
import express from "express"
import CompetencesController from "./controllers/CompetencesController";
import HomeController from "./controllers/HomeController";
import CriteresController from "./controllers/CriteresController";
import UserController from "./controllers/UserController";
import { redirectLogin } from "./server";
import { Request, ParamsDictionary, Response } from "express-serve-static-core";
import { body, validationResult } from "express-validator";

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/competences', (req, res) => {
        console.log(req.session)
        CompetencesController.competences(req, res);
    });

    app.get('/criteres/:id', redirectLogin, (req: Request, res: Response, next: any) => {
        req.params.id
        console.log(req.params.id)
        CriteresController.criteres(req, res);
    });

    app.post('/competences', function(req, res) {
        CriteresController.formulaire(req, res);
    });
// route pour recuperer des donnees pour le view 
    app.get('/register', (req, res) => {
        UserController.RegForm(req, res);
    })
// route pour enregistrer les donnees d'utilisateur 
    app.post('/register',
    body('email').isEmail().normalizeEmail(),
    body('password').isLength({
        min: 6
    }),
     (req:express.Request, res:express.Response) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(400).json({
                success: false,
                errors: errors.array()
            });
        }

     UserController.register(req, res);
    })

// route pour comparer les donnees d'utilisateur rentree avec la base de donnee
    app.post('/login', (req, res) => {
        UserController.login(req, res);
    })
// route pour recuperer le view de login form
    app.get('/login', (req, res) => {
        UserController.LogForm(req, res);
    })
// user login 
    app.post('/logout', (req, res) => {
        UserController.logout(req, res);
    })

   

}
