INSERT INTO "competences" ("activity_type", "title", "content")
VALUES (
        'Développer la partie front-end d’une application web ou web mobile en intégrant les recommandations de sécurité',
        'Maquetter une application',
        'Description de la compétence – processus de mise en œuvre
À partir de cas d’utilisation ou de scénarios utilisateur,
        de la charte graphique et des exigences de sécurité identifiées,
        concevoir la maquette des interfaces utilisateurs de l ’ application,
        avec du contenu en langue française ou anglaise,
        y compris celles appropriées à l’équipement ciblé et en tenant compte de l ’ expérience utilisateur et pour un équipement mobile des spécificités ergonomiques.Formaliser les enchaînements des interfaces afin que l ’ utilisateur les valide ainsi que les maquettes.Contexte(s) professionnel(s) de mise en œuvre Le maquettage intervient en phase de conception avec une forte implication de l’utilisateur final, actuel ou
futur, le plus souvent dans une démarche projet itérative centrée sur l’expérience utilisateur (approche Agile).Chaque itération permet d ’enrichir et de finaliser la maquette, afin que l’utilisateur valide l’interface
graphique de l’application et en retrouve les principales fonctionnalités tout au long du cycle de
développement. Les itérations peuvent donner lieu à des ateliers de présentation aux utilisateurs. Le
maquettage intervient pour des applications sur des supports et environnements multiples.'
    ),
    (
        'Développer la partie front -end d ’ une application web ou web mobile en intégrant les recommandations de sécurité ',
        ' Réaliser une interface utilisateur web statique et adaptable ',
        ' Description de la compétence – processus de mise en œuvre À partir de la maquette de l ’ interface à réaliser,
    de la charte graphique et à l’aide d’un langage de présentation,
    créer les pages web statiques et adaptables,
    y compris pour les équipements mobiles,
    afin d ’ obtenir un rendu visuel optimisé et adapté à l’équipement de l’utilisateur et à l’ensemble des navigateurs ciblés.Respecter les bonnes pratiques de structuration et de sécurité ainsi que les contraintes de l’architecture du
matériel cible.
Publier les pages web sur un serveur et les rendre visibles sur les moteurs de recherche.
Rechercher des solutions pertinentes pour la résolution de problèmes techniques de rendu et
d’accessibilité en utilisant de la documentation en langue française ou anglaise.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s’exerce seul,
        ou en équipe quand l ’ ergonomie du site nécessite l’apport de
compétences en web design pour la mise en page des informations et/ou des objets graphiques et
multimédias. La compétence peut nécessiter l’utilisation d’un environnement de développement et d’un
environnement de test pour les équipements ciblés y compris mobiles. La visibilité du site (référencement)
dépend du public visé.'
    ),
    (
        'Développer la partie front-end d’une application web ou
web mobile en intégrant les recommandations de
sécurité',
        'Développer une interface utilisateur web dynamique',
        'Description de la compétence – processus de mise en œuvre
À partir de l’interface utilisateur web statique et adaptable,
    de la charte graphique et dans le respect des bonnes pratiques de développement et de sécurisation d’application web : développer, tester et
documenter les traitements coté client, afin d’obtenir une page web dynamique et d’améliorer l’expérience
utilisateur en particulier pour les équipements mobiles.
Optimiser l’application web pour une utilisation sur les équipements mobiles.
Publier l’application web et la rendre visible sur les moteurs de recherche.
Pratiquer une veille technologique, y compris en anglais, pour résoudre un problème technique ou mettre
en œuvre une nouvelle fonctionnalité ainsi que pour s’informer sur la sécurité informatique et les
vulnérabilités connues.
Partager le résultat de sa veille avec ses pairs.
Contexte(s) professionnel(s) de mise en œuvre
Cette compétence s’exerce seul ou en équipe et peut nécessiter l’utilisation d’un environnement de développement.Le rendu visuel et les fonctionnalités doivent être vérifiés sur l’ensemble des navigateurs ciblés.La visibilité de l’application web (référencement) dépend du public visé.L’optimisation d’application web pour les équipements mobiles peut requérir l’utilisation des mécanismes asynchrones (de type AJAX,...).'
    ),
    (
        ' Développer la partie front -
end d’une application web ou web mobile en intégrant les recommandations de sécurité ',
        ' Réaliser une interface utilisateur avec une solution de gestion de contenu ou e - commerce ',
        ' Description de la compétence – processus de mise en œuvre À partir d’un besoin client,
installer,
paramétrer ou maintenir une solution de gestion de contenu ou d’e - commerce afin de réaliser de manière facilitée un site vitrine ou une boutique en ligne,
créer les différents comptes utilisateurs avec leurs droits et rôles dans le respect des règles de sécurité,
créer ou adapter la structure du site ou de la boutique afin de permettre à l’utilisateur d’en gérer le contenu et d’en suivre l’activité.Personnaliser l’aspect visuel du site ou de la boutique afin de respecter la charte graphique du client et d’être adapté à l’équipement de l’utilisateur,
y compris mobile,
et à l’ensemble des navigateurs ciblés.Publier le site vitrine ou la boutique en ligne sur un serveur et le (la) rendre visible sur les moteurs de recherche.Pratiquer une veille technologique,
y compris en anglais,
pour résoudre un problème technique ou mettre en œuvre une nouvelle fonctionnalité ainsi que pour s ’ informer sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s’exerce seul ou en équipe. L’installation et le paramétrage se font via des pages
d’administration des solutions de gestion de contenu ou d’e-commerce.'
    ),
    (
    'Développer la partie back-end d’une application web
ou web mobile en intégrant les recommandations de
sécurité',
    'Créer une base de données',
    'Description de la compétence – processus de mise en œuvre
À partir d’une demande client nécessitant le stockage de données, organiser les données et définir un
schéma physique. A l’aide d’un SGBD, écrire et exécuter le script de création de la base de données,
insérer les données de test, définir les droits d’utilisation et prévoir les procédures de sauvegarde et de restauration de la base de données de test.Conformément à l ’ état de l ’ art de la sécurité et aux exigences de sécurité identifiées,
    exprimer le besoin de sécurité du SGDB afin de permettre l ’ élaboration d ’ une base de données sécurisée.Rechercher,
    en utilisant de la documentation en langue française ou anglaise,
    des solutions pertinentes pour la résolution de problèmes techniques et de nouveaux usages,
    notamment des bases de données non relationnelles.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Les bases de données relationnelles sont utilisées dans les applications traditionnelles où les données sont centralisées sur un serveur.Les données sont amenées à être souvent mises à jour.Les bases de données non relationnelles sont utilisées dans les cas de traitement de données massives à l’échelle du Web. Les données sont essentiellement lues, il y a peu de mises à jour.
Dans le cas d’une demande d’évolution applicative et à partir d’une base de données existante,
    il s ’ agira de modifier le schéma physique.Les différents scripts de création de base de données,
    de gestion des droits,
    d’insertion de données et
d’exécution de sauvegarde et restauration sont exécutés sur un serveur supportant un système de gestion de base de données.Les besoins de sécurité du SGBD sont exprimés par le développeur et le paramétrage est effectué par un administrateur de bases de données.'
),
(
    ' Développer la partie back -
end d ’ une application web ou web mobile en intégrant les recommandations de sécurité ',
    ' Développer les composants d ’ accès aux données ' , ' Description de la compétence – processus de mise en œuvre À partir du dossier de conception technique et d ’ une bibliothèque d ’ objets spécialisés dans l ’ accès aux données,
coder,
tester et documenter les composants d ’accès aux données stockées dans une base de
données afin d’opérer des sélections et des mises à jour de données nécessaires à une application
informatique et de façon sécurisée.
Rechercher, éventuellement en langue anglaise, des solutions innovantes et pertinentes pour la résolution
de problèmes techniques et de nouveaux usages, notamment d’accès aux données non relationnelles.Pratiquer une veille technologique sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa recherche ou de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s ’exerce seul ou en équipe et concerne le développement de la partie persistance de
l’ application.Le développement des composants s ’ effectue à partir d’un environnement de développement
intégré, supportant un langage objet en liaison avec une base de données relationnelles ou non
relationnelles. L’accès aux données passe par l’utilisation d’un logiciel d’interface (middleware).
Les données peuvent être dans un contexte de base de données relationnelles ou non relationnelles.'
),
(
    'Développer la partie back-end d’une application web
ou web mobile en intégrant les recommandations de
sécurité',
    'Développer la partie back-end d’une application web ou web mobile',
    'Description de la compétence – processus de mise en œuvre
À partir des fonctionnalités décrites dans le dossier de conception technique, et dans le respect des
bonnes pratiques de développement et de sécurisation d’application web,
    coder,
    tester et documenter les traitements côté serveur,
    afin d ’ assurer la collecte,
    le traitement et la restitution d ’ informations numériques.Publier l ’ application web sur un serveur.Pratiquer une veille technologique,
    y compris en anglais,
    pour résoudre un problème technique ou mettre en œuvre une nouvelle fonctionnalité ainsi que pour s ’ informer sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s’exerce seul ou en équipe. Le développement d’une application web s ’ effectue à partir d’un environnement de développement intégré, supportant les différents langages serveur.
L’application web est optimisée pour les équipements mobiles à travers la mise en place de mécanismes
asynchrones de chargement et restitution de données (Ajax, ...).'
),
(
    'Développer la partie back-end d’une application web
ou web mobile en intégrant les recommandations de
sécurité',
    'Elaborer et mettre en œuvre des composants dans une
application de gestion de contenu ou e-commerce',
    'Description de la compétence – processus de mise en œuvre
À partir du cahier des charges fonctionnel et du système de gestion de contenu ou d’e - commerce,
    dans le respect des bonnes pratiques de développement,
    de la solution logicielle,
    intégrer ou coder,
    tester et documenter des modules complémentaires afin de rendre le site web adapté aux besoins des utilisateurs,
    en respectant à chaque étape l ’ état de l ’ art de la sécurité informatique.Publier l ’ application web sur un serveur.Pratiquer une veille technologique,
    y compris en anglais,
    pour résoudre un problème technique ou mettre en œuvre une nouvelle fonctionnalité ainsi que pour s ’ informer sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s ’exerce seul ou en équipe. Le développement d’une application web s ’ effectue à partir d ’un environnement de développement intégré, supportant les différents langages serveur.'
);


INSERT INTO "Criteres" ("Content", "Competences_ID")VALUES 
        ('La maquette prend en compte les spécificités fonctionnelles décrites dans les cas d’ utilisation ou les scénarios utilisateur ',1 ), 
        ('L ’enchaînement des écrans est formalisé par un schéma', 1),
        ('La maquette et l’ enchaînement des écrans sont validés par l ’ utilisateur ',1),
        ('La maquette respecte la charte graphique de l ’ entreprise ', 1),
        ('La maquette est conforme à l ’expérience utilisateur et à l’équipement ciblé',1),
        ('La maquette respecte les principes de sécurisation d’une interface utilisateur',1),
        ('La maquette prend en compte les exigences de sécurité spécifiques de l’application',1),
        ('Le contenu de la maquette est rédigé, en français ou en anglais, de façon adaptée à l’interlocuteur et sans, faute',1),

        ('L ’ interface est conforme à la maquette de l ’ application ', 2),
        ('Les pages web respectent la charte graphique de l ’ entreprise ',2),
        ('Les bonnes pratiques de structuration et de sécurité sont respectées y compris pour le web mobile',2), 
        ('Les pages web sont accessibles depuis les navigateurs ciblés y compris depuis un mobile ',2),
        ('Les pages web s ’ adaptent à la taille de l ’ écran ',2),
        ('Les pages web sont optimisées pour le web mobile',2), 
        ('Le site respecte les règles de référencement naturel' , 2), 
        ('Les pages web sont publiées sur un serveur ',2),
        ('L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise' , 2),
        ('La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre - sens,...)' , 2), 
        ('La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité' , 2),
        ('Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs ' , 2),

        ('Les pages web respectent la charte graphique de l ’ entreprise', 3), 
        ('Les pages web sont conformes à l ’ expérience utilisateur y compris pour l ’ expérience mobile.', 3),
        ('L ’ architecture de l ’ application répond aux bonnes pratiques de développement et de sécurisation d ’ application web', 3), 
        ('L ’ application web est optimisée pour les équipements mobiles', 3), 
        ('Le code source est documenté ou auto - documenté', 3), 
        ('L ’ application web est publiée sur un serveur', 3), 
        ('Les tests garantissent que les pages web répondent aux exigences décrites dans le cahier des charges', 3), 
        ('Les tests de sécurité suivent un plan reconnu par la profession', 3), 
        ('L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise', 3), 
        ('La documentation technique liée aux technologies associées, en français ou en anglais,est comprise (sans contre - sens,...)', 3), 
        ('La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité', 3), 
        ('La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles', 3), 
        ('Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs ', 3) , 
    
        ('Le site est installé et paramétré conformément au besoin client',4) , 
        ('Les comptes utilisateurs sont créés avec leurs droits et rôles dans le respect des règles de sécurité',4) , 
        ('La structure du site est conforme au besoin client',4) , 
        ('L ’ aspect visuel respecte la charte graphique du client et est adapté à l ’ équipement de l ’ utilisateur',4) , 
        ('Le site est publié sur un serveur',4) , 
        ('Le site respecte les règles de référencement naturel',4) , 
        ('L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise',4) , 
        ('La documentation technique liée aux technologies associées,en français ou en anglais, est comprise (sans contre - sens,...)',4), 
        ('La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité',4) , 
        ('La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles',4) , 
        ('Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs ' ,4) ,
    
        ('La base de données est conforme au schéma physique Le script de création de bases de données s ’ exécute sans erreurs ' ,5) , 
        ('Le script d ’ insertion des données de test s ’ exécute sans erreurs La base de données est disponible avec les droits d ’ accès prévus ' ,5), 
        ('La base de données de test peut être restaurée en cas d ’ incident ' ,5) , 
        ('Les besoins de sécurité du SGBD sont exprimés selon l ’ état de l ’ art et les exigences de sécurité identifiées ' ,5) , 
        ('L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise ' ,5) , 
        ('La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre - sens,...)  ' ,5) ,
        ('La démarche de recherche permet de trouver une solution à un problème technique ' ,5) , 
        ('La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité ' ,5) , 
        ('Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs ' ,5) , 
    
        ('Les traitements relatifs aux manipulations des données répondent aux fonctionnalités décrites dans le dossier de conception technique ' ,6) , 
        ('Un test unitaire est associé à chaque composant, avec une double approche fonctionnelle et sécurité ' ,6) , 
        ('Le code source des composants est documenté ou auto - documenté ' ,6) , 
        ('Les composants d ’ accès à la base de données suivent les règles de sécurisation reconnues ' ,6) , 
        ('La sécurité des composants d ’ accès se fonde sur les mécanismes de sécurité du SGBD ' ,6) , 
        ('L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise ' ,6) , 
        ('La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre - sens,...) ' ,6) , 
        ('La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité ' ,6) , 
        ('La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles ' ,6) , 
        ('Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs ' ,6) , 
    
        ('Les bonnes pratiques de développement sont respectées ' ,7) , 
        ('Les composants serveur contribuent à la sécurité de l ’ application ' ,7) , 
        ('Le code source des composants est documenté ou auto - documenté ' ,7) , 
        ('Les tests garantissent que les traitements serveurs répondent aux fonctionnalités décrites dans le dossier de conception technique ' ,7) , 
        ('Les tests de sécurité suivent un plan reconnu par la profession ' ,7) , 
        ('L ’ application web est publiée sur un serveur ' ,7) , 
        ('L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise ' ,7) , 
        ('La documentation technique liée aux technologies associées, y compris en anglais, est comprise (sans contre - sens,...) ' ,7) , 
        ('La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité ' ,7) , 
        ('La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles ' ,7) , 
        ('Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs  ' ,7) ,

        ('Les bonnes pratiques de développement objet sont respectées ',8) , 
        ('Les composants complémentaires ou réalisés s ’ intègrent dans l ’ environnement de l ’ application ',8) , 
        ('Les composants serveur contribuent à la sécurité de l ’ application ',8) , 
        ('Le code source des composants est documenté ou auto - documenté ',8) , 
        ('Les tests garantissent que les traitements serveurs répondent aux fonctionnalités décrites dans le cahier des charges ',8) , 
        ('Les tests de sécurité suivent un plan reconnu par la profession ',8) , 
        ('L ’ application web est publiée sur un serveur ',8) , 
        ('L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise ',8) , 
        ('La documentation technique liée aux technologies associées, y compris en anglais, est comprise (sans contre - sens,...) ',8) , 
        ('La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité ',8) , 
        ('La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles ',8) , 
        ('Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs ',8);
    
    INSERT INTO "user" ("username" , "email", "password") VALUES 
    ( 'defaultUser' , 'defaultUser@email.com' , 'defaultpassword');

    INSERT INTO "devWebDescrip" ("title" , "content")  VALUES 
    ('Développeur web et web mobile' , 
    'Définition de l’emploi type et des conditions d’exercice
Le développeur web et web mobile développe ou fait évoluer des applications orientées web, ou web
mobile ou les deux à la fois, en respectant les normes et standards reconnus par la profession et en
suivant l’état de l’art de la sécurité informatique à toutes les étapes. Ces applications sont destinées à des
internautes ou des salariés d’entreprise.
Le développeur conçoit la partie visuelle de l’application exécutée sur le navigateur côté ordinateur client. Il
ébauche la maquette des écrans de l’interface utilisateur et prévoit la navigation de l’application web ou
web mobile. Il code l’ensemble avec les langages de programmation web adaptés et une architecture
applicative conforme aux bonnes pratiques du développement web et web mobile. Il réalise la partie
visuelle de l’application dans un premier temps de manière statique et en la rendant adaptable au contexte
puis dans un deuxième temps capable d’interagir avec l’utilisateur de manière dynamique.
En fonction des besoins de l’utilisateur, il peut installer et adapter une solution préétablie de gestion de
contenu ou d’e-commerce.
Il respecte les principes de sécurisation des interfaces utilisateur et prend en compte la charte graphique,
ou les interfaces utilisateur standardisées, ainsi que les règles d’accessibilité.
À la suite ou en parallèle au développement de la partie visuelle, le développeur conçoit et code la partie
back-end, c’est-à-dire exécutée côté serveur pour accéder aux données. Le développeur crée la base de
données et développe les composants d’accès aux données pour pouvoir les afficher et les mettre à jour
dans l’application web ou web mobile.
Si le choix a été fait de mettre en œuvre une solution de gestion de contenu ou d’e-commerce, il
développe des modules complémentaires pour adapter la solution choisie et installée au préalable.
Il s’adapte en continu aux évolutions technologiques du secteur du développement web et web mobile.
Pour assurer cette veille, l’usage de la langue anglaise est souvent requis pour la lecture et la
compréhension de documentations techniques ainsi que pour assurer des échanges techniques au moyen
de textes courts avec des développeurs distants pouvant être de nationalités différentes.
Il peut travailler éventuellement à distance en tant que salarié d’une entreprise, ou pour un client de la
société de services qui l’emploie, ou en tant qu’indépendant directement pour un client. Ses activités
diffèrent selon la taille de l’entreprise. En PME, il peut mener seul le développement d’un projet. Dans une
grande structure, il travaille soit au sein d’une équipe hiérarchisée sous la responsabilité d’un chef de
projet, soit en équipe pluridisciplinaire. Il applique les normes de qualité logicielle de son entreprise ou de
son prestataire de services. Il applique les recommandations de sécurité émises par l’Agence Nationale de
la Sécurité des Systèmes d’Information (ANSSI).
Les environnements de développement et de test sont prédéfinis par un Lead Developer ou un chef de
projet.
Il participe aux réunions de projet et aux ateliers de présentations utilisateurs.
Dans le cadre d’un développement en approche de type Agile, il échange avec l’utilisateur ou un
responsable de maîtrise d’ouvrage. Il est encadré éventuellement par un développeur plus expérimenté
(lead developer).
Dans le cadre d’un développement en approche classique, il n’a pas d’échange direct avec l’utilisateur et il
est encadré par un chef de projet.
Le développeur travaille en équipe. Il utilise notamment des outils de gestion de version et des outils
collaboratifs. Selon le contexte, il collabore avec d’autres développeurs, un ergonome, un web designer,
un architecte logiciel, un testeur et avec le responsable de la sécurité des systèmes d’information (RSSI)
de son entreprise, de son client ou de son hébergeur.
Assurant sa mission dans des entreprises et des contextes professionnels divers, il est mobile
géographiquement et s’adapte aux nouveaux environnements de travail.');