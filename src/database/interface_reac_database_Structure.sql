-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Teodor
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-10 14:33
-- Created:       2021-12-06 14:00
PRAGMA foreign_keys = OFF;

-- Schema: mydb
BEGIN;
CREATE TABLE "user"(
  "ID" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(16) NOT NULL,
  "email" VARCHAR(255) NOT NULL,
  "password" VARCHAR(32) NOT NULL
);
CREATE TABLE "devWebDescrip"(
  "ID" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(45) NOT NULL,
  "content" VARCHAR(45) NOT NULL
);
CREATE TABLE "Competences"(
  "ID" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "activity_type" VARCHAR(45) NOT NULL,
  "title" VARCHAR(255) NOT NULL,
  "Content" VARCHAR(45) NOT NULL
);
CREATE TABLE "Criteres"(
  "ID" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "Content" VARCHAR(45) NOT NULL,
  "Competences_ID" INTEGER NOT NULL,
  CONSTRAINT "fk_Criteres_Competences1"
    FOREIGN KEY("Competences_ID")
    REFERENCES "Competences"("ID")
);
CREATE INDEX "Criteres.fk_Criteres_Competences1_idx" ON "Criteres" ("Competences_ID");
CREATE TABLE "user_has_Criteres"(
  "ID" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "user_ID" INTEGER NOT NULL,
  "Criteres_ID" INTEGER NOT NULL,
  CONSTRAINT "fk_user_has_Criteres_user"
    FOREIGN KEY("user_ID")
    REFERENCES "user"("ID"),
  CONSTRAINT "fk_user_has_Criteres_Criteres1"
    FOREIGN KEY("Criteres_ID")
    REFERENCES "Criteres"("ID")
);
CREATE INDEX "user_has_Criteres.fk_user_has_Criteres_Criteres1_idx" ON "user_has_Criteres" ("Criteres_ID");
CREATE INDEX "user_has_Criteres.fk_user_has_Criteres_user_idx" ON "user_has_Criteres" ("user_ID");
COMMIT;
